var mongoose = require("mongoose");
var Bicicleta = require("../../models/bicicleta");
var request = require("request");
var server = require("../../bin/www");

var base_url = "http://localhost:5000/api/bicicletas";

describe("Bicicleta API", () => {
  beforeEach(function (done) {
    var mongoDB = "mongodb://localhost/red_bicicletas";
    mongoose.connect(mongoDB, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    });

    const db = mongoose.connection;
    db.on("error", console.error.bind(console, "connection error"));
    db.once("open", function () {
      console.log("we are connected to test database");
      done();
    });
  });

  afterEach(function (done) {
    Bicicleta.deleteMany({}, function (err, success) {
      if (err) console.log(err);
      done();
    });
  });

  describe("POST BICICLETAS /create", () => {
    it("Status 200", (done) => {
      var headers = {
        "Content-Type": "application/json"
      };
      var aBici =
        '{"id":10, "color": "rojo", "modelo": "urbana", "lat": -34, "lng": -54}';
      request.post({
          headers: headers,
          url: base_url + "/create",
          body: aBici,
        },
        function (error, response, body) {
          expect(response.statusCode).toBe(200);
          var bici = JSON.parse(body).bicicletas;

          expect(bici.color).toBe("rojo");
          expect(bici.ubicacion[0]).toBe(-34);
          expect(bici.ubicacion[1]).toBe(-54);
          done();
        }
      );
    });
  });

  describe("GET BICICLETAS", () => {
    it("Status 200", (done) => {
      request.get(base_url, function (error, response, body) {
        var result = JSON.parse(body);
        expect(response.statusCode).toBe(200);
        expect(result.bicicletas.length).toBe(0);
        done();
      });
    });
  });

  describe("POST BICICLETAS /update", () => {
    it("Status 200", (done) => {
      var headers = {
        "Content-Type": "application/json"
      };
      var aBici =
        '{"id":1, "color": "morado", "modelo": "todoterreno", "lat": 4.5555, "lng": -74.444}';
      var a = Bicicleta.createInstance(1, "rojo", "urbana", [
        4.6671738,
        -74.0916038,
      ]);
      Bicicleta.add(a);
      request.post({
          headers: headers,
          url: "http://localhost:5000/api/bicicletas/update",
          body: aBici,
        },
        function (error, response, body) {
          expect(response.statusCode).toBe(200);
          Bicicleta.findByCode(1, function (req, bici) {
            expect(bici.color).toBe("morado");
            done();
          });
        }
      );
    });
  });

  describe("POST BICICLETAS /delete", () => {
    it("Status 200", (done) => {
      var headers = {
        "Content-Type": "application/json"
      };
      var aBici = '{"id":1}';
      var a = Bicicleta.createInstance(1, "rojo", "urbana", [
        4.6671738,
        -74.0916038,
      ]);
      Bicicleta.add(a);
      request.delete({
          headers: headers,
          url: "http://localhost:5000/api/bicicletas/delete",
          body: aBici,
        },
        function (error, response, body) {
          expect(response.statusCode).toBe(204);
          Bicicleta.allBicis(function (err, bicicletas) {
            expect(bicicletas.length).toBe(0);
            done();
          });
        }
      );
    });
  });
});