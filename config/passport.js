const passport = require("passport");
const LocalStrategy = require("passport-local").Strategy;
const Usuario = require("../models/usuario");

passport.use(
  new LocalStrategy(function (email, password, done) {
    Usuario.findOne({
      email: email
    }, function (err, usuario) {
      if (err) return done(err);
      if (!usuario)
        return done(null, false, {
          message: "Email no existente o incorrecto"
        });
      if (!usuario.validatePassword(password))
        return done(null, false, {
          message: "Password incorrecto"
        });

      return done(null, usuario);
    });
  })
);


module.exports = function(app){
  app.use(passport.initialize());
  app.use(passport.session());
  passport.serializeUser(function(Usuario, done){
      done(null, Usuario);
  });
  passport.deserializeUser(function (Usuario, done) {
     done(null, Usuario);
  });
  google();
};


passport.serializeUser(function (usuario, cb) {
  cb(null, usuario.id);
});

passport.deserializeUser(function (id, cb) {
  Usuario.findById(id, function (err, usuario) {
    cb(err, usuario);
  });
});

module.exports = passport;