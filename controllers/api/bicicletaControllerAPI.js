var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');

exports.bicicleta_list = function (req, res) {
    Bicicleta.allBicis(function (err, bicis) {
        res.status(200).json({
            bicicletas: bicis
        });
    });
};

exports.bicicleta_create = function (req, res) {
    var bicicleta = Bicicleta.createInstance(req.body.id, req.body.color, req.body.modelo, [req.body.lat, req.body.lng]);

    Bicicleta.add(bicicleta, function (err, newBici) {
        res.status(200).json({
            bicicletas: newBici
        });
    });
}

exports.bicicleta_delete = function (req, res) {
    Bicicleta.removeByCode(req.body.id, function (err, code) {
        res.status(204).send();
    });
}

exports.bicicleta_update = function (req, res) {
    var bici = Bicicleta.createInstance(req.body.id, req.body.color, req.body.modelo, [req.body.lat, req.body.lng]);
    Bicicleta.update(bici, function (err, result) {
        if (err) console.log(err)
        res.status(200).json({
            bicicleta: bici
        });
    });
}