var mymap = L.map('main_map').setView([4.6403825, -74.1345402], 13);

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1Ijoic2VyZ2lvZ3I1IiwiYSI6ImNrOG5hOHN4dDA0NmQzbW15eXp2dmh1eGEifQ.wAO5ZGlgBaMrp3AYipOKqw'
}).addTo(mymap);

$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function(result){
        console.log(result);
        result.bicicletas.forEach(function(bici){
            L.marker(bici.ubicacion, {title: bici.id}).bindPopup(`<b>Bici ${bici.id}</b><br>Rodando`).addTo(mymap);
        });
    }
})